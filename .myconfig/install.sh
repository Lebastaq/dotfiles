git clone --bare https://gitlab.com/Lebastaq/dotfiles.git $HOME/.config
function config {
   /usr/bin/git --git-dir=$HOME/.config/ --work-tree=$HOME $@
}
mkdir -p .config-backup && \
config checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | \
xargs -I{} mv {} .config-backup/{}
config checkout

curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
