" recursive mapping because we want to use the mapping defined in ruby.vim
nmap <buffer> <leader>im [m
nmap <buffer> <leader>em ]m
nmap <buffer> <leader>iM [M
nmap <buffer> <leader>eM ]M

vmap <leader>im [m
vmap <leader>em ]m
vmap <leader>iM [M
vmap <leader>eM ]M
