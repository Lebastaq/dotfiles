set expandtab
set shiftwidth=2
set noshowmode
set softtabstop=2
set number relativenumber
set smartindent
set autoindent
set showcmd
set laststatus=2
set rtp +=~/.fzf

set background=dark
set termguicolors
set timeout timeoutlen=1000 ttimeoutlen=0

let g:python_host_prog='/usr/bin/python2'

" Autoinstall vim-plug
if empty(glob('~/.nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall
endif

call plug#begin()
Plug 'tpope/vim-surround'
Plug 'tpope/vim-fugitive'

" Themes
Plug 'phanviet/Sidonia'
Plug 'junegunn/seoul256.vim'
Plug 'NLKNguyen/papercolor-theme'

Plug 'airblade/vim-gitgutter'
let g:gitgutter_async = 0
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
let g:airline_theme='base16'
let g:airline_section_x = ''
let g:airline_section_y = ''
let g:airline_section_z = ''
Plug 'nathanaelkane/vim-indent-guides'
let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_start_level = 2
let g:indent_guides_guide_size = 1
Plug 'vim-ruby/vim-ruby'
Plug 'terryma/vim-multiple-cursors'
Plug 'majutsushi/tagbar'
call plug#end()

colorscheme sidonia

" remaps
let g:mapleader = " "
nnoremap <SPACE> <Nop>

noremap <silent> <leader><space> :FZF<CR>
noremap <silent> <leader>s :w<CR>

noremap h t
noremap j s
noremap k r
noremap l n

noremap t h
noremap n l
noremap r k
noremap s j

" tag navigation
nnoremap <silent><leader>f <C-]>
nnoremap <silent><leader>fe g<C-]>

" remap method finding
nnoremap <leader>im [m

" easier buffer navigation
nnoremap <silent><leader>ib :bprevious<CR>
nnoremap <silent><leader>eb :bnext<CR>
noremap <silent><leader>iB :bfirst<CR>
noremap <silent><leader>eB :blast<CR>
nnoremap <silent><leader>b :ls<CR>

" Auto commands
" Automatically delete whitespace
autocmd BufWritePre * %s/\s\+$//e