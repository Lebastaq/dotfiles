set expandtab
set shiftwidth=2
set noshowmode
set softtabstop=2
set number relativenumber
set smartindent
set autoindent
set showcmd
set laststatus=2
set rtp +=~/.fzf
set encoding=utf8

set background=dark
set termguicolors
set timeout timeoutlen=1000 ttimeoutlen=0

" Autoinstall vim-plug
if empty(glob('~/.nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  "autocmd VimEnter * PlugInstall
endif

call plug#begin('~/.local/share/nvim/plugged')
Plug 'tpope/vim-surround'
"Plug 'tpope/vim-fugitive'

" Themes
Plug 'phanviet/Sidonia'
Plug 'junegunn/seoul256.vim'
Plug 'NLKNguyen/papercolor-theme'
Plug 'YorickPeterse/happy_hacking.vim'

" Other plugins
Plug 'airblade/vim-gitgutter', { 'for': ['ruby', 'python'] }
let g:gitgutter_async = 0
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
let g:airline_theme='base16'
let g:airline_section_x = ''
let g:airline_section_y = ''
let g:airline_section_z = ''
Plug 'nathanaelkane/vim-indent-guides'
let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_start_level = 2
let g:indent_guides_guide_size = 1
Plug 'vim-ruby/vim-ruby', { 'for': 'ruby' }
"Plug 'terryma/vim-multiple-cursors'
"Plug 'majutsushi/tagbar'
"Plug 'python-mode/python-mode', { 'branch': 'develop', 'for': 'python' }
Plug 'davidhalter/jedi-vim'
Plug 'Valloric/YouCompleteMe', { 'branch': 'develop', 'for': 'python' }
Plug 'kshenoy/vim-signature'
Plug 'Raimondi/delimitMate'
"Plug 'SirVer/ultisnips'
"Plug 'honza/vim-snippets'
"Plug 'nvie/vim-flake8', {'for': 'python'}
Plug 'tell-k/vim-autopep8'
Plug 'majutsushi/tagbar', {'for': 'python'}
call plug#end()


colorscheme happy_hacking
" airline config
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '>'
let g:airline#extensions#tabline#formatter = 'unique_tail'

" remaps
let g:mapleader = " "
nnoremap <SPACE> <Nop>

noremap <silent> <leader><space> :FZF<CR>
noremap <silent> <leader>s :w<CR>

noremap h t
noremap j s
noremap k r
noremap l n

noremap t h
noremap n l
noremap r k
noremap s j

" tag navigation
nnoremap <silent><leader>f <C-]>
nnoremap <silent><leader>fe g<C-]>

" remap method finding
nnoremap <leader>im [m

" easier buffer navigation
nnoremap <silent><leader>ib :bprevious<CR>
nnoremap <silent><leader>eb :bnext<CR>
noremap <silent><leader>iB :bfirst<CR>
noremap <silent><leader>eB :blast<CR>
nnoremap <silent><leader>b :ls<CR>:b<Space>

" easier tab navigation
nnoremap <silent><leader>it :tabprevious<CR>
nnoremap <silent><leader>et :tabnext<CR>
noremap <silent><leader>iT :tabfirst<CR>
noremap <silent><leader>eT :tablast<CR>
nnoremap <C-t>     :tabnew<CR>

" easier window navigation
nnoremap <silent><leader>w <C-w>w

" Auto commands
" Automatically delete whitespace
autocmd BufWritePre * %s/\s\+$//e

" Python options
"let g:pymode_python = 'python3'
"nnoremap <buffer><silent><leader>r :exec '!python' shellescape(@%, 1)<CR>

" Snippets config
"let g:UltiSnipsExpandTrigger="<leader><e>"
"let g:UltiSnipsJumpForwardTrigger="<c-b>"
"let g:UltiSnipsJumpBackwardTrigger="<c-z>"
